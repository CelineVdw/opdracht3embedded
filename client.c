#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

int main(int argc, char *argv[])
{
    int sockfd, portno, n,byteswritten=0,written;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    char fileLocation[100];
    char fileName[50];
    char buffer[1024];
    FILE *fp;
    
    if (argc < 5) {
       fprintf(stderr,"usage %s hostname port fileLocation\n", argv[0]);
       exit(0);
    }
    
    portno = atoi(argv[2]);
    strcpy(fileLocation,argv[3]);
    strcpy(fileName,argv[4]);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    
    if (sockfd < 0) 
        error("ERROR opening socket");
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }

    //zero out buffer (serv_addr now empty)
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    //copy server adress via byte copy
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
         //portno instellen als argument
    serv_addr.sin_port = htons(portno);
    //connect to socket
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
        error("ERROR connecting");
    
    //set buffer to zero and send filename
    bzero(buffer,1024);
    n = write(sockfd,fileName,strlen(fileName)+1);
    if (n < 0) 
         error("ERROR writing to socket");
    n = read(sockfd,buffer,1023);
    if (n < 0) 
         error("ERROR reading from socket");
    printf("filename %s set\n",buffer);
         //create content
    
    printf("Opening file : %s\n",fileLocation);
    //open file and get size
    fp = fopen(fileLocation, "r");
    fseek(fp, 0L, SEEK_END);
    int sz = ftell(fp);
    //set buffer to zero and send filesize
    bzero(buffer,1024);
    char tempSize[50];
    bzero(tempSize,50);
    sprintf(tempSize,"%i",sz);
    n = write(sockfd,tempSize,strlen(tempSize));
    if (n < 0) 
         error("ERROR writing to socket");
    n = read(sockfd,buffer,1023);
    if (n < 0) 
         error("ERROR reading from socket");
    printf("filesize %s set\n",buffer);
    rewind(fp);
    //set buffer to zero
    char sendBfr[sz];
    bzero(sendBfr,sz);
    //send filesize

    //place value in buffer
    fgets(sendBfr,sz,fp);
    printf("%s sending\n",buffer);
    //write to socket
    n = write(sockfd,sendBfr,sz);
    //set buffer to zero
    bzero(buffer,1024);
    //read only executed after write is finished
    n = read(sockfd,buffer,1023);
    if (n < 0) 
         error("ERROR reading from socket");
    //output filelocation as feedback
    printf("%s sent\n",buffer);
    //close socket and file
    fclose(fp);
    close(sockfd);
    return 0;
}
