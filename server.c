/* A simple server in the internet domain using TCP
   The port number is passed as an argument 
   This version runs forever, forking off a separate 
   process for each connection
*/
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
//time header added for time stuff
#include <time.h>
void dostuff(int); /* function prototype */
void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int main(int argc, char *argv[])
{
    
     int sockfd, newsockfd, portno, pid;
     /
     socklen_t clilen;
     
     struct sockaddr_in serv_addr, cli_addr;
    
     if (argc < 2) {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
     }
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) 
        error("ERROR opening socket");
        
     bzero((char *) &serv_addr, sizeof(serv_addr));
     portno = atoi(argv[1]);
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);
     
     if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0) 
              error("ERROR on binding");
     listen(sockfd,5); //start listening.
     clilen = sizeof(cli_addr);
     
     while (1) {
         newsockfd = accept(sockfd, //accept new socket
               (struct sockaddr *) &cli_addr, &clilen);
         if (newsockfd < 0) //error if failed
             error("ERROR on accept");
         pid = fork(); //fork it
         if (pid < 0)
             error("ERROR on fork"); //error if failed
         if (pid == 0)  {
             close(sockfd); //close socket, execute dostuff with the new socket we opened.
             dostuff(newsockfd);
             exit(0);
         }
         else close(newsockfd);
     } /* end of while */
     close(sockfd);
     return 0; 
}

/******** DOSTUFF() *********************
 There is a separate instance of this function 
 for each connection.  It handles all communication
 once a connnection has been established.
 *****************************************/
void dostuff (int sock)
{
    time_t current_time;
    struct tm *info_p;
    struct tm info;
    char filepath[50];
    char filename[50];
    int filesize;
   int n;
   char buffer[1024];
          //create new filepointer
    FILE *fd;
   bzero(buffer,1024);

   if (n < 0) error("ERROR reading from socket");
    
    current_time = time(NULL);
    info_p = localtime(&current_time);

    
    read(sock,filename,49);
    write(sock,filename,sizeof(filename));
    
    read(sock,buffer,50);
    filesize = atoi(buffer);
    write(sock,buffer,sizeof(buffer));
    
    strftime(filepath,sizeof(filepath),"./received%Y%m%d%H%M", info_p);
    strcat(filepath,filename);
    printf("%s\n",filepath);
    
    
    fd = fopen(filepath, "w+");
    n=read(sock,buffer,1024);
    if(fd != NULL)
    {
        fputs(buffer, fd);
        printf("File saved \n");
    }
    else
      {
        printf("Error writing file \n");
      }
    fclose(fd); //close file
   n = write(sock,"I got your file",18); //return message to confirm file transfer.
   if (n < 0) error("ERROR writing to socket");
}
